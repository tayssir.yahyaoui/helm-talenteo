
$chartName = Read-Host "Please provide the new Helm chart name"
$containerImageTag = Read-Host "Please provide the desired container image tag from ACR"
$chartVersion = Read-Host "Please provide the new chart version"
$serviceTCPPort = Read-Host "Please provide service TCP Port"
$hostName = Read-Host "Please provide configured host name"
$hostPath = Read-Host "Please provide configured host path"
$environement = Read-Host "Please provide environment"
$chartSourcePath = "./helm-chart-definitions"
$chartValuesPath = "./helm-chart-values"
$packagedChartsPath= "./helm-chart-packages"
$chartTemplatePath= "./helm-chart-template"
$chartTemplateTest= "templates/tests/"
$newChartPath="$chartSourcePath/$chartName"
$newChartValuesFileName="$chartName-values-$environement.yaml"

If (!(Test-Path -Path $newChartPath)) 
{
    Copy-Item "$chartTemplatePath" "$newChartPath" -Recurse
    Get-ChildItem "$newChartPath" -File -Recurse | %{(Get-Content $_.FullName).Replace('<SERVICE_NAME>', "$chartName") | Set-Content $_.FullName}
    (Get-Content "$newChartPath/Chart.yaml").Replace('<CHART_VERSION>', "$chartVersion") | Set-Content "$newChartPath/Chart.yaml"
    (Get-Content "$newChartPath/values.yaml").Replace('<CONTAINER_IMAGE_TAG>', "$containerImageTag") | Set-Content "$newChartPath/values.yaml"
    (Get-Content "$newChartPath/values.yaml").Replace('<SERVICE_TCP_PORT>', "$serviceTCPPort") | Set-Content "$newChartPath/values.yaml"
    (Get-Content "$newChartPath/values.yaml").Replace('<HOST_NAME>', "$hostName") | Set-Content "$newChartPath/values.yaml"
    (Get-Content "$newChartPath/values.yaml").Replace('<HOST_PATH>', "$hostPath") | Set-Content "$newChartPath/values.yaml"
    (Get-Content "$newChartPath/values.yaml").Replace('<ENVIRONMENT>', "$environement") | Set-Content "$newChartPath/values.yaml"
    (Get-Content "$chartTemplatePath/$chartTemplateTest/test-connection.yaml").Replace('helm-chart-template', "$chartName") | Set-Content "$newChartPath/$chartTemplateTest/test-connection.yaml"
    Move-Item "$newChartPath/values.yaml" "$chartValuesPath/$newChartValuesFileName"
    helm lint "$newChartPath" --values "$chartValuesPath/$newChartValuesFileName"
    helm package "$newChartPath" -d "$packagedChartsPath"
    helm repo index "$packagedChartsPath"

}

